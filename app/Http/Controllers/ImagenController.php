<?php

namespace App\Http\Controllers;

use App\Establecimiento;
use App\Imagen;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ImagenController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {         

        // Leer la imagen  y almacen en public    
        $ruta_imagen = $request->file('file')->store('establecimientos', 'public');                                                  

        // Resize a la imagen
        $imagen = Image::make( public_path("storage/{$ruta_imagen}"))->fit(800, 450);

        // Almacenar 
        $imagen->save();

        // Almacenar con modelo
        $imagenDB = new Imagen;
        $imagenDB->id_establecimiento = $request['uuid'];
        $imagenDB->ruta_imagen = $ruta_imagen;

        $imagenDB->save();

        // Retornar Respuesta
        $respuesta = [
            'archivo' => $ruta_imagen
        ];

        return response()->json($respuesta);
        //return $request->all();      
        //  Para obtener informacion de un archivo o imagen se usa return $request->file('file');     
        //return $request->file('file');                                                                                                                
    }                 
           
    /**
     * Display the specified resource.
     *
     * @param  \App\Imagen  $imagen
     * @return \Illuminate\Http\Response
     */
    public function show(Imagen $imagen)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Imagen  $imagen
     * @return \Illuminate\Http\Response
     */
    public function edit(Imagen $imagen)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Imagen  $imagen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Imagen $imagen)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Imagen  $imagen
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        // Obtner el uuid enviado desde dropzone.js
        $uuid = $request->get('uuid');
        $establecimiento = Establecimiento::where('uuid', $uuid)->first();
        $this->authorize('delete',$establecimiento);

        
        // Imagen a eliminar
        $imagen = $request->get('imagen');

         if(File::exists('storage/' . $imagen)) {
           // Elimina imagen del servidor
           File::delete('storage/' . $imagen);

           // elimina imagen de la BD
           Imagen::where('ruta_imagen', $imagen )->delete();

           $respuesta = [
                'mensaje' => 'Imagen Eliminada',
                'imagen' => $imagen,
                'uuid' => $uuid
            ];
       }

        return response()->json($respuesta);
    }
}


