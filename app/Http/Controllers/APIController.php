<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Establecimiento;
use App\Imagen;
use Illuminate\Http\Request;

class APIController extends Controller
{
    // Metdod para obtener todos los establecimientos
    public function index()
    {
        $establecimientos = Establecimiento::with('categoria')->get();
        return response()->json($establecimientos);
    }
    // Metodo para obtener todas las categorias
    public function categorias()
    {
        $categorias = Categoria::all();
        // Como es una api, la respuesta tiene que estar en json
        return response()->json($categorias);
    }

    // Muestra los establecimientos de una categoria en especifico
    function categoria(Categoria $categoria)
    {
                                                                                    /* Cargarle los valores de la relacion de categoria y establecimiento */
        $establecimientos = Establecimiento::where('categoria_id' , $categoria->id)->with('categoria')->take(3)->get();

        return response()->json($establecimientos);
    }
    
    // Obtener todos los establecimientos segun una categoria
    public function establecimientoCategoria(categoria $categoria)
    {
        $establecimientos = Establecimiento::where('categoria_id' , $categoria->id)->with('categoria')->get();

        return response()->json($establecimientos);
    }

    // Muestra un establecimiento en especifico
    public function show(Establecimiento $establecimiento)
    {
        $imagenes = Imagen::where('id_establecimiento', $establecimiento->uuid)->get();
        // Se crea establecimiento->imagenes
        $establecimiento->imagenes = $imagenes;
        return response()->json($establecimiento);
    }
}
