<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    //Leer las rutas por slug, cuando pase una Categoria $categoria a la funcion, en vez de buscar por id, como acostumbra que busque por slug
    public function getRouteKeyName()
    {
        return 'slug';
    }

    // Relacion de 1:n Muchos establecimientos pertenecen a una categoria
    public function establecimientos()
    {
        return $this->hasMany(Establecimiento::class);
    }
}
