@extends('layouts.app')

@section('styles')
      <!-- Load Leaflet from CDN -->
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
  integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
  crossorigin=""/>

    <!-- Load Esri Leaflet Geocoder from CDN -->
    <link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.css"
    integrity="sha512-IM3Hs+feyi40yZhDH6kV8vQMg4Fh20s9OzInIIAc4nx7aMYMfo+IenRUekoYsHZqGkREUgx0VvlEsgm7nCDW9g=="
    crossorigin="">

    <!-- Dropzone -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/dropzone.min.css" integrity="sha512-0ns35ZLjozd6e3fJtuze7XJCQXMWmb4kPRbb+H/hacbqu6XfIX0ZRGt6SrmNmv5btrBpbzfdISSd8BAsXJ4t1Q==" crossorigin="anonymous" />   
@endsection

@section('content')
    <div class="container">
        <h1 class="text-center mt-"> Editar Establecimiento</h1>
        <div class="mt-5 row justify-content-center">
            <form class="col-md-9 col-xs-12 card card-body" action="{{ route('establecimiento.update', ['establecimiento'=> $establecimiento->id])}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <fieldset class="border p-4">
                    <legend class="text-primary">Nombre, Categoria e Imagen Principal</legend>

                    <!--- Nombre Establecimiento --->
                    <div class="form-group">
                        <label for="nombre">Nombre Establecimiento</label>
                        <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" placeholder="Nombre Establecimiento" name="nombre" value="{{ $establecimiento->nombre}}">

                        @error('nombre')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>    
                        @enderror
                    </div>

                    <!--- Categoria --->
                    <div class="form-group">
                        <label for="categoria">Categoria</label>
                        <select class="form-control  @error('categoria_id') is-invalid @enderror" name="categoria_id" id="categoria">
                            <option value="" selected disable>-- Seleccione --</option>
                            @foreach ($categorias as $categoria)
                                <option value="{{$categoria->id}}"
                                    {{$establecimiento->categoria_id == $categoria->id ? 'selected' : '' }}>{{$categoria->nombre}}</option>
                            @endforeach
                        </select>
                    </div>

                    <!--- Imagen Principal --->
                    <div class="form-group">
                        <label for="imagen_principal">Nombre Establecimiento</label>
                        <input id="imagen_principal" type="file" class="form-control @error('imagen_principal') is-invalid @enderror" name="imagen_principal" value="{{old('imagen_principal')}}">

                        @error('imagen_principal')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>    
                        @enderror
                        <!-- Imagen lod -->
                        <img style="width:400px; margin-top:10px" src="/storage/{{ $establecimiento->imagen_principal}}">
                    </div>
                </fieldset>

                <fieldset class="border p-4">
                    <legend class="text-primary">Ubicacion </legend>

                    <!--- Direcccion Establecimiento OJO SE ENVIARA A UNA API DE UN MAPA--->
                    <div class="form-group">
                        <label for="formbuscador">Coloca la direccion del Establecimiento</label>
                        <!-- Como no se enviara a la DB no es ncesario el name ni value --->
                        <input id="formbuscador" type="text" class="form-control" placeholder="Calle del Negocio o Establecimiento">
                        <p class="text-secondary mt-5 mb-3 text-center">El asistente colocará una dirección  estimada, mueve el pin hacia el lugar correcto</p>
                    </div>

                    <!-- Contenedor del Mapa -->
                    <div class="form-group">
                        <div id ="mapa" style="height: 400px;"></div>
                    </div>

                    
                    <p class="informacion">Confirma que los siguientes campos son correctos</p>
                    <!--- Direccion --->
                    <div class="form-group">
                        <label for="direccion">Direccion</label>
                        <input type="text" id="direccion" class="form-control @error('direccion') is-invalid @enderror" placeholder="Dirección" value="{{$establecimiento->direccion}}" name="direccion">
                        @error('direccion')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>    
                        @enderror
                    </div>
                    <!--- Colonia --->
                    <div class="form-group">
                        <label for="colonia">Colonia</label>
                        <input type="text" id="colonia" class="form-control @error('colonia') is-invalid @enderror" placeholder="Colonia" value="{{$establecimiento->colonia}}" name="colonia">
                        @error('colonia')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>    
                        @enderror
                    </div>

                    <!--- Latitud y longitu en inputs ocultos --->
                    <input type="hidden" id="lat" name="lat" value="{{$establecimiento->lat}}">
                    <input type="hidden" id="lng" name="lng" value="{{$establecimiento->lng}}">
                </fieldset>

                <fieldset class="border p-4 mt-5">
                    <legend  class="text-primary">Información Establecimiento: </legend>
                        <div class="form-group">
                            <label for="nombre">Teléfono</label>
                            <input 
                                type="tel" 
                                class="form-control @error('telefono')  is-invalid  @enderror" 
                                id="telefono" 
                                placeholder="Teléfono Establecimiento"
                                name="telefono"
                                value="{{ $establecimiento->telefono }}"
                            >
    
                                @error('telefono')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                        </div>
    
                        
    
                        <div class="form-group">
                            <label for="nombre">Descripción</label>
                            <textarea
                                class="form-control  @error('descripcion')  is-invalid  @enderror" 
                                name="descripcion">
                                {{ $establecimiento->descripcion }}</textarea>
    
                                @error('descripcion')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                        </div>
    
                        <div class="form-group">
                            <label for="nombre">Hora Apertura:</label>
                            <input 
                                type="time" 
                                class="form-control @error('apertura')  is-invalid  @enderror" 
                                id="apertura" 
                                name="apertura"
                                value="{{ $establecimiento->apertura }}"
                            >
                            @error('apertura')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
    
                        <div class="form-group">
                            <label for="nombre">Hora Cierre:</label>
                            <input 
                                type="time" 
                                class="form-control @error('cierre')  is-invalid  @enderror" 
                                id="cierre" 
                                name="cierre"
                                value="{{ $establecimiento->cierre }}"
                            >
                            @error('cierre')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                </fieldset>

                <fieldset class="border p-4 mt-5">
                    <legend  class="text-primary">Imágenes Establecimiento: </legend>
                        <div class="form-group">
                            <label for="imagenes">Imagenes</label>
                            <div id="dropzone" class="dropzone form-control"></div>
                        </div>

                        @if(count($imagenes) > 0)
                            @foreach($imagenes as $imagen)
                                <input class="galeria" type="hidden" value="{{$imagen->ruta_imagen}}">
                            @endforeach
                        @endif

                </fieldset>

                <input type="hidden" id="uuid" name="uuid" value="{{ $establecimiento->uuid}}">
                <input type="submit" class="btn btn-primary mt-3 d-block" value="Guardar Cambios">

            
            </form>
        </div>
    </div>
@endsection

@section('scripts')
<!-- Dropzone -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/min/dropzone.min.js" integrity="sha512-Mn7ASMLjh+iTYruSWoq2nhoLJ/xcaCbCzFs0ZrltJn7ksDBx+e7r5TS7Ce5WH02jDr0w5CmGgklFoP9pejfCNA==" crossorigin="anonymous" defer></script>

<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
    integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
    crossorigin="" defer></script>

  <!-- Load Esri Leaflet from CDN -->
  <script src="https://unpkg.com/esri-leaflet@2.5.3/dist/esri-leaflet.js"
    integrity="sha512-K0Vddb4QdnVOAuPJBHkgrua+/A9Moyv8AQEWi0xndQ+fqbRfAFd47z4A9u1AW/spLO0gEaiE1z98PK1gl5mC5Q=="
    crossorigin="" defer></script>

    <script src="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.js"
    integrity="sha512-HrFUyCEtIpxZloTgEKKMq4RFYhxjJkCiF5sDxuAokklOeZ68U2NPfh4MFtyIVWlsKtVbK5GD2/JzFyAfvT5ejA=="
    crossorigin="" defer></script>

    <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"></script>

    
@endsection