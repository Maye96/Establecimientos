import Vue from 'vue';
import Vuex from 'vuex';


Vue.use(Vuex);
// Los states: Es donde se declaran variables que puedes ser usadas en todos los componentes
// Dichas Variables tienes mutations: los cuales son el cambio de valor de state que sigue un patron
export default new Vuex.Store({
    state: {
        cafes: [],
        hoteles: [],
        restaurants: [],
        establecimiento: {}, // Objeto para establecimiento
        establecimientos: [],
        categorias: [],
        categoria: ''
    },
    mutations: {
        VER_CAFES(state, cafes)
        {
            state.cafes = cafes;
        },
        VER_HOTELES(state, hoteles)
        {
            state.hoteles = hoteles;
        },
        VER_RESTAURANTS(state, restaurants)
        {
            state.restaurants = restaurants;
        },
        AGREGAR_ESTABLECIMIENTO(state, establecimiento)
        {
            state.establecimiento = establecimiento;
        },
        AGREGAR_ESTABLECIMIENTOS(state, establecimientos)
        {
            // Asignar al state establecimientos el valor que se pasa por el commit en el axios
            state.establecimientos = establecimientos;
        },
        AGREGAR_CATEGORIAS(state, categorias) {
            state.categorias = categorias;
        },
        SELECCIONAR_CATEGORIA(state, categoria) {
            state.categoria = categoria;
        },
    },
    // El  valor del establecimiento se le puede pasar al componnete tambien a traves de un getter
    getters: {
        obtenerEstablecimiento: state => {
            return state.establecimiento;
        },
        obtenerImagenes: state =>{
            return state.establecimiento.imagenes;
        },
        obtenerEstablecimientos: state => {
            return state.establecimientos;
        },
        obtenerCategorias: state => {
            return state.categorias;
        },
        obtenerCategoria: state => {
            return state.categoria;
        }

    }
});