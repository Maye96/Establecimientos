import Vue from 'vue';
import VueRouter from 'vue-router';
import VuePageTransition from 'vue-page-transition';
import InicioEstablecimientos from '../components/InicioEstablecimientos'
import MostrarEstablecimiento from '../components/MostrarEstablecimiento'

// Se instala vue-router dentro de Vue 
Vue.use(VueRouter);
Vue.use(VuePageTransition);

const routes = [
    {
        // Ruta para acceder al componente
        path:'/',
        // Componente al que se quiere acceder
        component: InicioEstablecimientos
    },
    {
        path:'/establecimiento/:id',
        name:'establecimiento',
        component: MostrarEstablecimiento
    }

]

const router = new VueRouter({
    // Para que en la ruta no se muestre /#/. http://127.0.0.1:8000/#/
    mode: 'history',
    routes
});

export default router;