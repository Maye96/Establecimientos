const { default: Axios } = require("axios");

document.addEventListener('DOMContentLoaded', () => {

    // Verificar si existe el id dropzone, porque no en todos lados estara
    if(document.querySelector('#dropzone')) {

        Dropzone.autoDiscover = false;

        // Definir en donde conseguira el dropzone
        const dropzone = new Dropzone('div#dropzone',{
            url: '/imagenes/store',
            // Mensaje en el dropzone
            dictDefaultMessage: 'Sube hasta 10 imagenes',
            // Limita la cantidad maxima de archivos
            maxFiles: 10,
            // Indicar que es oblogatorio
            required: true,
            // Indicar que formatos seran aceptados
            acceptedFiles: ".png, .jpg, .gif, .bmp, .jpeg",
            // Habilitar la opcion de eliminar las imagenes
            addRemoveLinks: true,
            // Personalizar mensaje de eliminar las imagenes
            dictRemoveFile: "Eliminar Imagen",
            // Para enviar el dropzone con el csrf, Laravel necesita el token para el post
            headers: {
                'X-CSRF-TOKEN': document.querySelector('meta[name=csrf-token]').content
            },
            // Metodo para llenar la galeria en dropzone
            init: function() {
                // Obtener las imagenes en el input hidden de galeria
                const galeria = document.querySelectorAll('.galeria');

                console.log('galeria');
                console.log(galeria);
                console.log(galeria.length);

                if(galeria.length > 0 ) {
                    // For para las imagenes
                    galeria.forEach(imagen => {

                        const imagenPublicada = {};
                        imagenPublicada.size = 1;
                        // Asignar el nombre de la imagen
                        imagenPublicada.name = imagen.value;
                        // Nombre de la imagen que se maneja internamente
                        imagenPublicada.nombreServidor = imagen.value;

                        // Agregar imagenes a dropzone
                        this.options.addedfile.call(this, imagenPublicada);
                        this.options.thumbnail.call(this, imagenPublicada, `/storage/${imagenPublicada.name}`);

                        // Agregar clases de dropzone para agregar icono de check
                        imagenPublicada.previewElement.classList.add('dz-success');
                        imagenPublicada.previewElement.classList.add('dz-complete');

                    })
                }
            },
            // Respuesta de la peticion
            success: function(file, respuesta)
            {
                console.log(file);
                console.log(respuesta);
                // Nombre del archivo
                file.nombreServidor= respuesta.archivo;
            },
            // Lo que se envia al servidor, se ejecuta automaticamente cuando envias algo hacia  '/imagenes/store' 
            sending: function (file, xhr, formData) 
            {
                console.log('Enviando ...');
                //En formData se agrega lo que se va enviar
                                /* LLave, Valor */
                formData.append('uuid', document.querySelector('#uuid').value )
            
            }, 
            // Funcion para remover imagenes
            removedfile: function(file, respuesta) 
            {
                console.log(file);
                const params = {
                    imagen: file.nombreServidor,
                    // Se pasa al destroy el uuid 
                    uuid: document.querySelector('#uuid').value 
                }
                axios.post('/imagenes/destroy', params)
                    .then(respuesta => {
                        console.log('Imagen a eliminar')
                        console.log(respuesta)

                        //Eliminar img del DOM
                        file.previewElement.parentNode.removeChild(file.previewElement);
                    })
            }
        });

    }

    

})